/**
 * (c) Copyright 2017 - Paul CIESLAR & Romain WALLON.
 * 
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements. See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.gitlab.mountolympus.hecate.sync;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * The ReadWriteMutex is a {@link Mutex} enabling concurrent readings.
 *
 * @author Paul CIESLAR
 * @author Romain WALLON
 *
 * @version 1.0
 */
public class ReadWriteMutex implements Locker {

    /**
     * The current number of readers.
     */
    private volatile AtomicInteger readerCount = new AtomicInteger();

    /**
     * The mutex for the reading actions.
     */
    private volatile Mutex readMutex = new Mutex();

    /**
     * The mutex for the writing actions.
     */
    private volatile Mutex writeMutex = new Mutex();

    /**
     * Locks the reading access to a resource.
     */
    public void lockRead() {
        readMutex.lock();
        if (readerCount.incrementAndGet() == 1) {
            // This is the first reader.
            writeMutex.lock();
        }
        readMutex.unlock();
    }

    /*
     * (non-Javadoc)
     * 
     * @see hecate.sync.Locker#lock()
     */
    @Override
    public void lock() {
        writeMutex.lock();
    }

    /*
     * (non-Javadoc)
     * 
     * @see hecate.sync.Locker#unlock()
     */
    @Override
    public void unlock() {
        writeMutex.unlock();
    }

    /**
     * Unlocks the reading access to a resource.
     */
    public void unlockRead() {
        readMutex.lock();
        if (readerCount.decrementAndGet() == 0) {
            // There's no more reader.
            writeMutex.unlock();
        }
        readMutex.unlock();
    }

}
