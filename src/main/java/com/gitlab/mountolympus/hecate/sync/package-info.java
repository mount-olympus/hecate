/**
 * The com.gitlab.mountolympus.hecate.sync package provides some useful tools for
 * synchronization.
 *
 * @author Paul CIESLAR
 * @author Romain WALLON
 *
 * @version 1.0
 */

package com.gitlab.mountolympus.hecate.sync;
