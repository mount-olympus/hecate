/**
 * (c) Copyright 2017 - Paul CIESLAR & Romain WALLON.
 * 
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements. See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.gitlab.mountolympus.hecate.sync;

import java.util.concurrent.atomic.AtomicInteger;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * The WaitGroup enables to easily wait for a collection of threads to finish.
 *
 * @author Paul CIESLAR
 * @author Romain WALLON
 *
 * @version 1.0
 */
public final class WaitGroup {

    /**
     * The logger used to log interruptions.
     */
    private static final Logger LOGGER = Logger.getLogger("hecate");

    /**
     * The count of threads to wait for.
     */
    private volatile AtomicInteger count;

    /**
     * Creates a new WaitGroup.
     */
    public WaitGroup() {
        this.count = new AtomicInteger();
    }

    /**
     * Adds a single thread to wait for.
     */
    public synchronized void add() {
        add(1);
    }

    /**
     * Adds several threads to wait for.
     * 
     * @param delta The number of threads to wait for.
     */
    public synchronized void add(int delta) {
        count.addAndGet(delta);
    }

    /**
     * Specifies that a thread is finished.
     */
    public synchronized void done() {
        if (count.decrementAndGet() == 0) {
            notifyAll();
        }
    }

    /**
     * Waits for the set of threads to finish.
     */
    public synchronized void await() {
        while (count.get() > 0) {
            try {
                wait();

            } catch (InterruptedException e) {
                LOGGER.log(Level.INFO, e.getMessage(), e);
                Thread.currentThread().interrupt();
            }
        }
    }

}
