/**
 * (c) Copyright 2017 - Paul CIESLAR & Romain WALLON.
 * 
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements. See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.gitlab.mountolympus.hecate.sync;

import java.util.concurrent.Semaphore;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * The Mutex is a semaphore used for mutual exclusion.
 * 
 * It is an adapter for a {@link Semaphore}.
 *
 * @author Paul CIESLAR
 * @author Romain WALLON
 *
 * @version 1.0
 */
public class Mutex implements Locker {

    /**
     * The logger used to log interruptions.
     */
    private static final Logger LOGGER = Logger.getLogger("hecate");

    /**
     * The adapted semaphore.
     */
    private final Semaphore semaphore = new Semaphore(1);

    /*
     * (non-Javadoc)
     * 
     * @see hecate.sync.Locker#lock()
     */
    @Override
    public void lock() {
        try {
            semaphore.acquire();

        } catch (InterruptedException e) {
            LOGGER.log(Level.INFO, e.getMessage(), e);
            Thread.currentThread().interrupt();
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see hecate.sync.Locker#unlock()
     */
    @Override
    public void unlock() {
        semaphore.release();
    }

}
