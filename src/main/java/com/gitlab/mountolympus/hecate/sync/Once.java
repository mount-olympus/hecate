/**
 * (c) Copyright 2017 - Paul CIESLAR & Romain WALLON.
 * 
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements. See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.gitlab.mountolympus.hecate.sync;

/**
 * The Once class enables to execute a {@link Runnable} only once.
 *
 * @author Paul CIESLAR
 * @author Romain WALLON
 *
 * @version 1.0
 */
public final class Once {

    /**
     * If the runnable has been executed.
     */
    private volatile boolean done;

    /**
     * Runs the given {@link Runnable} if, and only if, this object has never run any
     * {@link Runnable}.
     * 
     * In other words, this method does nothing, unless it has never ever been called on
     * this object, in which case it performs the given {@link Runnable}.
     * 
     * @param runnable The {@link Runnable} to run.
     */
    public synchronized void run(Runnable runnable) {
        if (!done) {
            runnable.run();
            done = true;
        }
    }

}
