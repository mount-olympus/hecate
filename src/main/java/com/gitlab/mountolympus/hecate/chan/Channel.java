/**
 * (c) Copyright 2017 - Paul CIESLAR & Romain WALLON.
 * 
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements. See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.gitlab.mountolympus.hecate.chan;

import java.util.Iterator;
import java.util.Optional;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * The Channel class represents a communication channel that can be used between several
 * threads.
 * 
 * @param <T> The type of the objects sent through the channel.
 *
 * @author Paul CIESLAR
 * @author Romain WALLON
 *
 * @version 1.0
 */
public final class Channel<T> implements AutoCloseable, Iterable<T> {

    /**
     * The logger used to log interruptions.
     */
    private static final Logger LOGGER = Logger.getLogger("hecate");

    /**
     * The queue in which the sent objects are stored.
     */
    private volatile BlockingQueue<Optional<T>> queue;

    /**
     * If this channel has been closed.
     */
    private volatile boolean closed;

    /**
     * Creates a new Channel.
     */
    public Channel() {
        this.queue = new LinkedBlockingQueue<>();
        this.closed = false;
    }

    /**
     * Writes the given object in this channel.
     * 
     * @param object The object to write.
     * 
     * @return If the object has been correctly written.
     * 
     * @throws IllegalStateException If this channel has been closed.
     * @throws NullPointerException If object is null.
     */
    public boolean write(T object) {
        synchronized (queue) {
            if (object == null) {
                // A null pointer can not be sent through a channel.
                throw new NullPointerException("Can not write null to a channel.");
            }

            return write(Optional.of(object));
        }
    }

    /**
     * Writes an object, wrapped in an {@link Optional}, to this channel.
     * An empty {@link Optional} is passed as parameter to mark the end of the channel.
     * 
     * @param optional The {@link Optional} containing the object to write.
     * 
     * @return If the object has been correctly written.
     * 
     * @throws IllegalStateException If this channel has been closed.
     */
    private boolean write(Optional<T> optional) {
        synchronized (queue) {
            if (closed) {
                // No more object can be written to this channel.
                throw new IllegalStateException("Channel has been closed.");
            }

            // Writing the object to the queue.
            boolean added = queue.offer(optional);
            queue.notifyAll();
            return added;
        }
    }

    /**
     * Waits for an element to be added to this channel.
     */
    private void waitForElement() {
        synchronized (queue) {
            while (queue.isEmpty()) {
                try {
                    queue.wait();

                } catch (InterruptedException e) {
                    LOGGER.log(Level.INFO, e.getMessage(), e);
                    Thread.currentThread().interrupt();
                    return;
                }
            }
        }
    }

    /**
     * Reads an object from this channel.
     * It blocks until this channel contains an element.
     * 
     * @return The read object, or null if this channel is closed.
     */
    public T read() {
        synchronized (queue) {
            waitForElement();

            // Reading the next object.
            Optional<T> value = queue.peek();
            if (value.isPresent()) {
                queue.poll();
                return value.get();
            }

            // This channel is closed.
            return null;
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Iterable#iterator()
     */
    @Override
    public Iterator<T> iterator() {
        return new ChannelIterator<>(this);
    }

    /**
     * Checks out if this channel is empty.
     * 
     * @return If this channel is empty.
     */
    boolean isEmpty() {
        synchronized (queue) {
            waitForElement();
            return closed && !queue.peek().isPresent();
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.AutoCloseable#close()
     */
    @Override
    public void close() {
        synchronized (queue) {
            if (!closed) {
                write(Optional.empty());
                closed = true;
                queue.notifyAll();
            }
        }
    }

}
