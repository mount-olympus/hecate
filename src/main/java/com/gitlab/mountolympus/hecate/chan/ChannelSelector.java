/**
 * (c) Copyright 2017 - Paul CIESLAR & Romain WALLON.
 * 
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements. See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.gitlab.mountolympus.hecate.chan;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.function.Consumer;

import com.gitlab.mountolympus.hecate.routine.Routine;

/**
 * The ChannelSelector enables to wait on multiple channels.
 * 
 * A call to {@link ChannelSelector#run()} blocks until one of its channels is ready.
 * If there is more than one, a single one is executed, but which one is undetermined.
 *
 * @param <T> The type of the objects sent through the channels.
 * 
 * @author Paul CIESLAR
 * @author Romain WALLON
 *
 * @version 1.0.1
 */
public final class ChannelSelector<T> implements Routine {

    /**
     * The cases of this selector.
     */
    private final Map<Channel<T>, Consumer<T>> cases;

    /**
     * The default case of this selector.
     */
    private Optional<Runnable> defaultCase;

    /**
     * Creates a new ChannelSelector.
     */
    private ChannelSelector() {
        this.cases = new HashMap<>();
        this.defaultCase = Optional.empty();
    }

    /**
     * Creates a channel selector.
     * 
     * @param <T> The type of the objects sent through the waited channels.
     * 
     * @return The created channel selector.
     */
    public static <T> ChannelSelector<T> select() {
        return new ChannelSelector<>();
    }

    /**
     * Adds a channel to wait for.
     * 
     * @param channel The channel to add.
     * @param block The block to execute if this channel is the first to be available.
     * 
     * @return This channel selector.
     * 
     * @throws IllegalArgumentException If the channel has already been added.
     */
    public ChannelSelector<T> caseReady(Channel<T> channel, Consumer<T> block) {
        if (cases.containsKey(channel)) {
            throw new IllegalArgumentException("Duplicate channel.");
        }

        cases.put(channel, block);
        return this;
    }

    /**
     * Adds a default case to this selector
     * 
     * @param block The block to execute if no channel is available.
     * 
     * @return This channel selector.
     * 
     * @throws IllegalStateException If there is already a default case.
     */
    public ChannelSelector<T> defaultCase(Runnable block) {
        if (defaultCase.isPresent()) {
            throw new IllegalStateException("Only one default case is allowed on a selector.");
        }

        this.defaultCase = Optional.of(block);
        return this;
    }

    /**
     * Executes the block of code associated with the first available channel, or the
     * default one - if it exists - when no channel is available.
     * 
     * @throws IllegalStateException If the select does not finish normally, i.e. when
     *         an exception is thrown during the execution of the selected code.
     *         
     * @since 1.0.1
     */
    @Override
    public void run() {
        List<CompletableFuture<?>> futures = new LinkedList<>();

        // Submitting the cases to wait for.
        for (Entry<Channel<T>, Consumer<T>> aCase : cases.entrySet()) {
            futures.add(CompletableFuture.<Runnable>supplyAsync(() -> 
                    () -> aCase.getValue().accept(aCase.getKey().read())));
        }

        // Submitting the default case, if any.
        if (defaultCase.isPresent()) {
            futures.add(CompletableFuture.completedFuture(defaultCase.get()));
        }

        try {
            // Selecting the task, and executing it.
            CompletableFuture<?>[] array = futures.toArray(new CompletableFuture<?>[futures.size()]);
            Runnable selected = (Runnable) CompletableFuture.anyOf(array).get();
            selected.run();

        } catch (InterruptedException | ExecutionException e) {
            // An exception has been thrown while executing the select.
            throw new IllegalStateException("select statement did not finish normally.", e);
        }
    }

}
