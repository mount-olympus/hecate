/**
 * The com.gitlab.mountolympus.hecate.chan package contains the classes used to represent
 * communication channels.
 *
 * @author Paul CIESLAR
 * @author Romain WALLON
 *
 * @version 1.0
 */

package com.gitlab.mountolympus.hecate.chan;
