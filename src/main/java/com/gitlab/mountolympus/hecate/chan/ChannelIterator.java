/**
 * (c) Copyright 2017 - Paul CIESLAR & Romain WALLON.
 * 
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements. See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.gitlab.mountolympus.hecate.chan;

import java.util.Iterator;
import java.util.NoSuchElementException;

/**
 * The ChannelIterator enables to iterate through a {@link Channel}.
 * 
 * Iteration stops when the channel is both empty and closed.
 * When the channel is empty but not closed, then it blocks until a new element is written
 * to the channel.
 * 
 * @param <T> The type of the objects sent through the channel.
 *
 * @author Paul CIESLAR
 * @author Romain WALLON
 *
 * @version 1.0
 */
final class ChannelIterator<T> implements Iterator<T> {

    /**
     * The channel to iterate through.
     */
    private final Channel<T> channel;

    /**
     * Creates a new ChannelIterator.
     * 
     * @param channel The channel to iterate through.
     */
    public ChannelIterator(Channel<T> channel) {
        this.channel = channel;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.util.Iterator#hasNext()
     */
    @Override
    public synchronized boolean hasNext() {
        return !channel.isEmpty();
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.util.Iterator#next()
     */
    @Override
    public synchronized T next() {
        if (!hasNext()) {
            // There is not any next element.
            throw new NoSuchElementException();
        }
        
        return channel.read();
    }

}
