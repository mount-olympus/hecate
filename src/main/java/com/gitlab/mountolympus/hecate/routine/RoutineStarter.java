/**
 * (c) Copyright 2017 - Paul CIESLAR & Romain WALLON.
 * 
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements. See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.gitlab.mountolympus.hecate.routine;

import java.util.concurrent.CompletableFuture;

/**
 * The RoutineStarter utility class enables to run routines in Go style.
 *
 * @author Paul CIESLAR
 * @author Romain WALLON
 *
 * @version 1.0.1
 */
public final class RoutineStarter {

    /**
     * Disables instantiation.
     */
    private RoutineStarter() {
        // Nothing to do: utility class.
    }

    /**
     * Gives the number of CPUs available on the computer.
     * 
     * @return The number of CPUs available on the computer.
     */
    public static int numberOfCPU() {
        return Runtime.getRuntime().availableProcessors();
    }

    /**
     * Starts the given routine asynchronously
     * 
     * @param routine The routine to start.
     */
    public static void go(Routine routine) {
        go(routine::run, routine::defer);
    }

    /**
     * Runs the given block asynchronously, and then executes the deferred.
     * 
     * @param block The block to run.
     * @param deferred The deferred instructions to perform.
     * 
     * @since 1.0.1
     */
    public static void go(Runnable block, Runnable deferred) {
        CompletableFuture.runAsync(() -> defer(block, deferred));
    }

    /**
     * Runs the given block, and then executes the deferred one.
     * 
     * @param block The block to run.
     * @param deferred The deferred instructions to perform.
     */
    private static void defer(Runnable block, Runnable deferred) {
        try {
            // Running the block.
            block.run();

        } finally {
            // Doing the deferred action.
            deferred.run();
        }
    }

}
