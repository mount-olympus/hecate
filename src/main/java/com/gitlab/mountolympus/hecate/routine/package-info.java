/**
 * The com.gitlab.mountolympus.hecate.routine package contains the classes used to run
 * threads in Go style.
 *
 * @author Paul CIESLAR
 * @author Romain WALLON
 *
 * @version 1.0
 */

package com.gitlab.mountolympus.hecate.routine;
