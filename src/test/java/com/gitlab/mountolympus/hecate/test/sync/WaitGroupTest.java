/**
 * (c) Copyright 2017 - Paul CIESLAR & Romain WALLON.
 * 
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements. See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.gitlab.mountolympus.hecate.test.sync;

import static com.gitlab.mountolympus.hecate.routine.RoutineStarter.go;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.concurrent.atomic.AtomicInteger;

import org.junit.Test;

import com.gitlab.mountolympus.hecate.routine.RoutineStarter;
import com.gitlab.mountolympus.hecate.sync.WaitGroup;
import com.gitlab.mountolympus.hecate.test.AbstractHecateTest;

/**
 * The WaitGroupTest is the class used to test wait groups.
 *
 * @author Paul CIESLAR
 * @author Romain WALLON
 *
 * @version 1.0
 */
public final class WaitGroupTest extends AbstractHecateTest {

    /**
     * Tests if a wait group works with a single asynchronous routine.
     */
    @Test
    public void testSingleRoutineInGroup() {
        WaitGroup waitGroup = new WaitGroup();
        waitGroup.add();
        
        AtomicInteger integer = new AtomicInteger();
        assertEquals(0, integer.get());
        go(() -> integer.incrementAndGet(), waitGroup::done);
        
        waitGroup.await();
        assertEquals(1, integer.get());
    }
    
    /**
     * Tests if a wait group works with multiple asynchronous routines.
     */
    @Test
    public void testMultipleRoutineInGroup() {
        int numberOfCPU = RoutineStarter.numberOfCPU();
        assertTrue(numberOfCPU > 0);
        
        WaitGroup waitGroup = new WaitGroup();
        waitGroup.add(numberOfCPU);
        
        AtomicInteger integer = new AtomicInteger();
        assertEquals(0, integer.get());
        
        for (int i = 0; i < numberOfCPU; i++) {
            go(integer::incrementAndGet, waitGroup::done);
        }
        
        waitGroup.await();
        assertEquals(numberOfCPU, integer.get());
    }
    
}

