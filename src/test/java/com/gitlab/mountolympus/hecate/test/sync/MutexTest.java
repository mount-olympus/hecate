/**
 * (c) Copyright 2017 - Paul CIESLAR & Romain WALLON.
 * 
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements. See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.gitlab.mountolympus.hecate.test.sync;

import static com.gitlab.mountolympus.hecate.routine.RoutineStarter.go;
import static org.junit.Assert.assertEquals;

import java.util.concurrent.atomic.AtomicInteger;

import org.junit.Test;

import com.gitlab.mountolympus.hecate.sync.Mutex;
import com.gitlab.mountolympus.hecate.test.AbstractHecateTest;

/**
 * The MutexTest is the class used to test mutexes.
 *
 * @author Paul CIESLAR
 * @author Romain WALLON
 *
 * @version 1.0
 */
public final class MutexTest extends AbstractHecateTest {
    
    /**
     * Tests if a call to {@code Mutex.lock()} blocks until the mutex is unlocked.
     */
    @Test
    public void testLocking() {
        Mutex mutex = new Mutex();
        mutex.lock();
        
        AtomicInteger integer = new AtomicInteger();
        assertEquals(0, integer.get());
        go(() -> {
            integer.incrementAndGet();
            mutex.unlock();
        });
        
        mutex.lock();
        assertEquals(1, integer.get());
    }
    
}

