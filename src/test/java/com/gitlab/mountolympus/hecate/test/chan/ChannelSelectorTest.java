/*
 * This file is a part of the hecate.test.chan package.
 *
 * It contains the ChannelSelectorTest
 *
 * (c) Romain WALLON - Hecate.
 * All rights reserved.
 */

package com.gitlab.mountolympus.hecate.test.chan;

import static org.junit.Assert.assertEquals;

import java.util.concurrent.atomic.AtomicInteger;

import org.junit.Test;

import com.gitlab.mountolympus.hecate.chan.Channel;
import com.gitlab.mountolympus.hecate.chan.ChannelSelector;
import com.gitlab.mountolympus.hecate.routine.RoutineStarter;
import com.gitlab.mountolympus.hecate.test.AbstractHecateTest;

/**
 * The ChannelTest is the class used to test selector on channels.
 *
 * @author Paul CIESLAR
 * @author Romain WALLON
 *
 * @version 1.0
 */
public class ChannelSelectorTest extends AbstractHecateTest {

    /**
     * Tests that only one of the cases of a {@link ChannelSelector} is executed.
     */
    @Test
    public void testSelectExecutesOneAndASingleOneCase() {
        Channel<String> a = new Channel<>();
        Channel<String> b = new Channel<>();
        Channel<String> c = new Channel<>();
        
        RoutineStarter.go(() -> b.write("b"));
        RoutineStarter.go(() -> a.write("a"));
        RoutineStarter.go(() -> c.write("c"));
        
        AtomicInteger integer = new AtomicInteger();
        assertEquals(0, integer.get());
        
        ChannelSelector.<String>select()
                .caseReady(a, msg -> integer.incrementAndGet())
                .caseReady(b, msg -> integer.incrementAndGet())
                .caseReady(c, msg -> integer.incrementAndGet())
                .defaultCase(integer::incrementAndGet)
                .run();
        assertEquals(1, integer.get());
    }

    /**
     * Tests that an exception is thrown when multiple cases are defined for a single channel.
     */
    @Test(expected = IllegalArgumentException.class)
    public void testCanNotDefineMultipleCasesForASingleChannel() {
        Channel<String> channel = new Channel<>();
        ChannelSelector.<String>select()
                .caseReady(channel, msg -> System.out.println(msg))
                .caseReady(channel, msg -> System.out.println(msg));
    }

    /**
     * Tests that an exception is thrown when multiple default cases are defined.
     */
    @Test(expected = IllegalStateException.class)
    public void testCanNotDefineMultipleDefaultCases() {
        ChannelSelector.<String>select()
                .defaultCase(() -> System.out.println("Default case."))
                .defaultCase(() -> System.out.println("Default case."));
    }

}
