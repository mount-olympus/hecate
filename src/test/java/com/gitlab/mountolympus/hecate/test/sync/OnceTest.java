/**
 * (c) Copyright 2017 - Paul CIESLAR & Romain WALLON.
 * 
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements. See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.gitlab.mountolympus.hecate.test.sync;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.util.concurrent.atomic.AtomicInteger;

import org.junit.Test;

import com.gitlab.mountolympus.hecate.sync.Once;
import com.gitlab.mountolympus.hecate.test.AbstractHecateTest;

/**
 * The OnceTest is the class used to test onces.
 *
 * @author Paul CIESLAR
 * @author Romain WALLON
 *
 * @version 1.0
 */
public final class OnceTest extends AbstractHecateTest {

    /**
     * Test method for {@link com.gitlab.mountolympus.hecate.sync.Once#run(java.lang.Runnable)}.
     */
    @Test
    public void testRun() {
        Once once = new Once();
        AtomicInteger integer = new AtomicInteger();
        assertEquals(0, integer.get());

        once.run(integer::incrementAndGet);
        assertEquals(1, integer.get());

        once.run(() -> fail("This block must noy have been executed."));
    }

    /**
     * Test method for {@link com.gitlab.mountolympus.hecate.sync.Once#run(java.lang.Runnable)} in a concurrent
     * context.
     * 
     * @throws InterruptedException If an error occurs during the concurrent runs.
     */
    public void testConcurrentRun() throws InterruptedException {
        Once once = new Once();
        AtomicInteger integer = new AtomicInteger();
        assertEquals(0, integer.get());

        Thread thread1 = new Thread(() -> once.run(integer::incrementAndGet));
        Thread thread2 = new Thread(() -> once.run(integer::incrementAndGet));

        thread1.start();
        thread2.start();

        thread1.join();
        thread2.join();

        assertEquals(1, integer.get());
    }

}
