/**
 * (c) Copyright 2017 - Paul CIESLAR & Romain WALLON.
 * 
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements. See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.gitlab.mountolympus.hecate.test.chan;

import static com.gitlab.mountolympus.hecate.routine.RoutineStarter.go;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import java.util.Iterator;
import java.util.NoSuchElementException;

import org.junit.Test;

import com.gitlab.mountolympus.hecate.chan.Channel;
import com.gitlab.mountolympus.hecate.test.AbstractHecateTest;

/**
 * The ChannelTest is the class used to test communication channels.
 *
 * @author Paul CIESLAR
 * @author Romain WALLON
 *
 * @version 1.0
 */
public final class ChannelTest extends AbstractHecateTest {

    /**
     * Tests the correct use of a channel.
     */
    @Test
    public void testCorrectUse() {
        Channel<Integer> channel = new Channel<>();
        go(() -> writeIntegers(channel, 10));
        assertEquals(55, reduce(channel));
    }

    /**
     * Writes to the given channel the integers between 1 and {@code maxValue}.
     * 
     * @param channel The channel to write the integers to.
     * @param maxValue The maximum value to write.
     */
    private void writeIntegers(Channel<Integer> channel, int maxValue) {
        for (int i = 1; i <= maxValue; i++) {
            channel.write(i);
        }
        channel.close();
    }

    /**
     * Reduces the number written in the given channel by adding them together.
     * 
     * @param channel The channel to read the integers from.
     * 
     * @return The result of the reduction.
     */
    private int reduce(Channel<Integer> channel) {
        int sum = 0;
        for (Integer i : channel) {
            sum += i;
        }
        return sum;
    }

    /**
     * Tests that an exception is thrown when {@code null} is sent through a channel.
     */
    @Test(expected = NullPointerException.class)
    public void testCannotWriteNullToChannel() {
        @SuppressWarnings("resource")
        Channel<?> channel = new Channel<>();
        channel.write(null);
    }

    /**
     * Tests that an exception is thrown when an object is sent through a closed channel.
     */
    @Test(expected = IllegalStateException.class)
    public void testCannotWriteToClosedChannel() {
        Channel<Integer> channel = new Channel<>();
        channel.close();
        channel.write(0);
    }

    /**
     * Tests that {@code null} is returned when a call to {@link Channel#read()} is made
     * when the channel is both closed and empty.
     */
    @Test
    public void testNullIsReturnedWhenReadingFromClosedChannel() {
        Channel<?> channel = new Channel<>();
        channel.close();
        assertNull(channel.read());
    }

    /**
     * Tests that a {@link NoSuchElementException} is thrown when a call to
     * {@link Iterator#next()} is made on an iterator over a channel which is both closed
     * and empty.
     */
    @Test(expected = NoSuchElementException.class)
    public void testCanNotMoveToNextElementWhenItDoesNotExist() {
        Channel<?> channel = new Channel<>();
        channel.close();
        channel.iterator().next();
    }

    /**
     * Tests that the {@link Channel#close()} method can be called several times on a
     * channel.
     */
    @Test
    public void testCanCloseChannelSeveralTimes() {
        Channel<?> channel = new Channel<>();
        channel.close();
        channel.close();
    }

}
