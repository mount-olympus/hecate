/**
 * (c) Copyright 2017 - Paul CIESLAR & Romain WALLON.
 * 
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements. See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.gitlab.mountolympus.hecate.test.routine;

import static com.gitlab.mountolympus.hecate.routine.RoutineStarter.go;
import static org.junit.Assert.assertEquals;

import org.junit.Test;

import com.gitlab.mountolympus.hecate.chan.Channel;
import com.gitlab.mountolympus.hecate.test.AbstractHecateTest;

/**
 * The RoutineTest is the class used to test routines.
 *
 * @author Paul CIESLAR
 * @author Romain WALLON
 *
 * @version 1.0
 */
public final class RoutineTest extends AbstractHecateTest {

    /**
     * Tests if a routine is executed by a call to {@code go}.
     */
    @Test
    public void testGo() {
        Channel<String> channel = new Channel<>();
        go(() -> channel.write("OK"));
        assertEquals("OK", channel.read());
        channel.close();
    }

    /**
     * Tests if a deferred action is executed at the end of a routine launched by a call
     * to {@code go}.
     */
    @Test
    public void testGoWithDeferredAction() {
        try (Channel<String> channel = new Channel<>()) {
            go(() -> channel.write("OK1"), () -> channel.write("OK2"));
            assertEquals("OK1", channel.read());
            assertEquals("OK2", channel.read());
        }
    }

    /**
     * Tests if a deferred action is executed at the end of a routine launched by a call
     * to {@code go}, even if an exception occurs during the routine execution.
     */
    @Test
    public void testGoWithDeferredActionAndError() {
        try (Channel<String> channel = new Channel<>()) {
            go(() -> {
                throw new UnsupportedOperationException();
            }, () -> channel.write("OK"));
            assertEquals("OK", channel.read());
        }
    }

}
