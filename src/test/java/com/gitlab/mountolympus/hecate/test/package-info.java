/**
 * The com.gitlab.mountolympus.hecate.test package is the parent of all the packages
 * containing the Hecate's test cases.
 *
 * @author Paul CIESLAR
 * @author Romain WALLON
 *
 * @version 1.0
 */

package com.gitlab.mountolympus.hecate.test;
