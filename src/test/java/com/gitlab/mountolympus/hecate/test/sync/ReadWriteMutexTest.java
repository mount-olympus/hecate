/**
 * (c) Copyright 2017 - Paul CIESLAR & Romain WALLON.
 * 
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements. See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.gitlab.mountolympus.hecate.test.sync;

import static com.gitlab.mountolympus.hecate.routine.RoutineStarter.go;
import static org.junit.Assert.assertEquals;

import java.util.concurrent.atomic.AtomicInteger;

import org.junit.Test;

import com.gitlab.mountolympus.hecate.sync.ReadWriteMutex;
import com.gitlab.mountolympus.hecate.test.AbstractHecateTest;

/**
 * The ReadWriteMutexTest is the class used to test read-write mutexes.
 *
 * @author Paul CIESLAR
 * @author Romain WALLON
 *
 * @version 1.0
 */
public final class ReadWriteMutexTest extends AbstractHecateTest {

    /**
     * Tests if concurrent readings are permitted when a call to
     * {@link ReadWriteMutex#lockRead()} has been made and only readers are considered.
     */
    @Test
    public void testReadLocking() {
        ReadWriteMutex mutex = new ReadWriteMutex();
        mutex.lockRead();

        // The following call will generate a timeout if it blocks.
        mutex.lockRead();
    }

    /**
     * Tests if a call to {@code ReadWriteMutex.lock()} blocks until the mutex is
     * unlocked.
     */
    @Test
    public void testWriteLocking() {
        ReadWriteMutex mutex = new ReadWriteMutex();
        mutex.lock();

        AtomicInteger integer = new AtomicInteger();
        assertEquals(0, integer.get());
        go(() -> {
            integer.incrementAndGet();
            mutex.unlock();
        });

        mutex.lock();
        assertEquals(1, integer.get());
    }

    /**
     * Tests if a call to {@code ReadWriteMutex.lockRead()} blocks until the mutex is
     * unlocked.
     */
    @Test
    public void testWriteReadLocking() {
        ReadWriteMutex mutex = new ReadWriteMutex();
        mutex.lock();

        AtomicInteger integer = new AtomicInteger();
        assertEquals(0, integer.get());
        go(() -> {
            integer.incrementAndGet();
            mutex.unlock();
        });

        mutex.lockRead();
        assertEquals(1, integer.get());
    }
    
    /**
     * Tests if a call to {@code ReadWriteMutex.lock()} blocks until the mutex is
     * unlocked by readers.
     */
    @Test
    public void testReadWriteLocking() {
        ReadWriteMutex mutex = new ReadWriteMutex();
        mutex.lockRead();

        AtomicInteger integer = new AtomicInteger();
        assertEquals(0, integer.get());
        go(() -> {
            integer.incrementAndGet();
            mutex.unlockRead();
        });

        mutex.lock();
        assertEquals(1, integer.get());
    }

}
