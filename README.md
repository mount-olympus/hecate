# Hecate - A Concurrency API for Java

[![build status](https://gitlab.com/mount-olympus/hecate/badges/master/build.svg)](https://gitlab.com/mount-olympus/hecate/commits/master)
[![coverage report](https://gitlab.com/mount-olympus/hecate/badges/master/coverage.svg)](https://gitlab.com/mount-olympus/hecate/commits/master)

## Description

The **Hecate API** allows you to easily write a multi-threaded and concurrent
Java program, thanks to an implementation of [Go](https://golang.org/)'s idioms.

The following section shows some examples of how to use this API.
You can also have a look to our test cases for an illustration.

The Javadoc is available [here](https://mount-olympus.gitlab.io/hecate/).

> *Note*
>
> This API is **not** intended to be as efficient as Go.
> Its purpose is only to mimic its syntax.

## Overview

With **Hecate**, you retrieve in Java (1.8 or above) the following Go's idioms
for concurrency.

### Launching Routines

The `Routine` interface extends `Runnable` to allow you to declare a *deferred*
block of code.

To launch your `Routine` in Go's style, you need to add the following import.

```java
import static com.gitlab.mountolympus.hecate.routine.RoutineStarter.go;
```

Then, all you need to do is to call the `go` method to execute methods or blocks
of code asynchronously.

```java
public class RoutineExample {

    private void anAsynchronousMethod() {
        // ...
    }

    private void aDeferredMethod() {
        // ...
    }

    public void aMethod() {
        // Calling asynchronously a method.
        go(this::anAsynchronousMethod);

        // ...

        // Calling asynchronously a method, and deferring a call to an other
        // one.
        // The deferred method is executed once the other one is finished, and
        // whatever happens during its execution.
        go(this::anAsynchronousMethod, this::aDeferredMethod);

        // The previous statement is a shortcut for the following one.
        go(new Routine() {
            @Override
            public void run() {
                anAsynchronousMethod();
            }

            @Override
            public void defer() {
                aDeferredMethod();
            }
        });
    }
}
```

### Communicate through channels

To communicate between routines, you can use channels.

#### How to use a channel

```java
public class ChannelExample {

    public static void main(String[] args) {
        Channel<String> channel = new Channel<>();

        // This routine prints what is written in the channel.
        // A call to channel.read() blocks until the channel contains something.
        go(() -> System.out.println(channel.read()));

        // This routine writes a string to the channel.
        go(() -> channel.write("Hello, World !"));
    }

}
```

Channels are also iterable.
To enable iteration, you need to close the channel once there is nothing more to
write in it.
This can be achieved with a `try-with-resources` statement, or by a direct call
to `channel.close()`.

```java
public class ChannelIterationExample {

    public static void main(String[] args) {
        Channel<Integer> channel = new Channel<>();

        // This routine prints all what is written in the channel, and stops
        // once this channel is closed.
        go(() -> {
            for (int i : channel) {
                System.out.println(i);
            }
        });

        // This routine writes numbers in the channel.
        go(() -> {
            for (int i = 0; i < 100; i++) {
                channel.write(i);
            }

            // This statement is absolutely needed in this case.
            channel.close();
        });
    }

}
```

#### Select on Channels

The `ChannelSelector` enables to wait on multiple channels.

A call to `run()` on a selector blocks until one of its channels is ready.
If there is more than one, a single one is executed, but which one is
undetermined.

```java
public class ChannelSelectorExample {

    public static void main(String[] args) {
        Channel<String> a = new Channel<>();
        Channel<String> b = new Channel<>();
        Channel<String> c = new Channel<>();

        go(() -> a.write("a"));
        go(() -> b.write("b"));
        go(() -> c.write("c"));

        ChannelSelector.<String>select()
            // If a is the first to be available, its message will be printed.
            .caseReady(a, message -> System.out.println(message))

            // If b is the first to be available, its message will be printed.
            .caseReady(b, message -> System.out.println(message))

            // If c is the first to be available, its message will be printed.
            .caseReady(c, message -> System.out.println(message))

            // If no channel is available, this block will be executed.
            // Setting a default case make a select non-blocking.
            .defaultCase(() -> System.out.println("default"))

            // Executing the select.
            .run();
    }
}
```

**IMPORTANT: Only one case per channel is allowed, and at most one default case
can be set for a selector.**

Because it implements `Routine`, a channel selector can be directly run in
background by a call to `go`.

> *Note*
>
> The implementation of `ChannelSelector` is thread based, so there's no
> guarantee that it works as well as Go's `select`.

### Locking access to resources

There are two ways to put locks in your code.

#### Using Mutexes

Mutexes are the *traditional* way to put locks.
However, Go prefers to use **Channels** and **WaitGroups** to synchronize your
code.

##### Simple Mutexes

Dealing with mutexes is simple.
The following class is an example of how they can be used.

```java
public class MutexExample {

    private Mutex mutex = new Mutex();

    public void asynchronousMethod() {
        // ...
        mutex.lock();
        // Code here is executed in mutual exclusion.
        mutex.unlock();
        // ...
    }

    public void aMethod() {
        // ...
        go(this::asynchronousMethod);
        // ...
        go(this::asynchronousMethod);
        // ...
    }
}
```

##### Read-Write Mutexes

Read-write mutexes are mutexes enabling to have concurrent reads.

```java
public class ReadWriteMutexExample {

    private final ReadWriteMutex mutex = new ReadWriteMutex();

    private final List<Object> list = new ArrayList<>();

    private void writer() {
        for (;;) {
            Object object;
            // Do some hard stuff to instantiate object...

            mutex.lock();

            // Only the current routine as accessed to list here.
            // Adding the object to the list.
            list.add(object);

            mutex.unlock();
        }
    }

    private void reader() {
        for (;;) {
            mutex.lockRead();

            // Only readers have accessed to list here.
            for (Object object : list) {
                System.out.println(object);
            }
            
            mutex.unlockRead();
        }
    }

    public void readAndWrite() {
        // Starting the writer routines.
        go(this::writer);
        // ...
        go(this::writer);

        // Starting the reader routines.
        go(this::reader);
        // ...
        go(this::reader);
    }

}

```

#### Better than Mutexes: WaitGroups

A WaitGroup waits for a collection of routines to finish.
A call to `add` enables to update the number of routines to wait for.
Then, each of routines runs and calls `done()` when finished.
At the same time, `await()` can be used to block until all routines have
finished.

```java
public class WaitGroupExample {

    public static void main(String[] args) {
        int numberOfCPU = RoutineUtil.numberOfCPU();
        AtomicInteger integer = new AtomicInteger();

        // Setting up the wait group.
        WaitGroup waitGroup = new WaitGroup();
        waitGroup.add(numberOfCPU);

        for (int i = 0; i < numberOfCPU; i++) {
            // Don't forget to call done() on the wait group once finished!
            go(integer::incrementAndGet, waitGroup::done);
        }

        // Waiting for the end of the execution of all the routines.
        waitGroup.await();
    }

}
```

### Once

An instance of `Once` enables to executes a method or a block of code only once.

```java
public class OnceExample {

    public static void main(String[] args) {
        Once once = new Once();

        for (int i = 0; i < 10; i++) {
            // "Hello, World!" will be printed only once.
            go(() -> once.run(() -> System.out.println("Hello, World !")));
        }
    }

}
```

### Logging Management

When unexpected exceptions occur while executing our API, they are logged using 
[Java Logging API](https://docs.oracle.com/javase/8/docs/technotes/guides/logging/overview.html).

Because you may want to manage yourself the logging, you can get access to our Logger by calling the 
following method.

```java
Logger hecateLogger = Logger.getLogger("hecate");
```

Then, all you have to do is to set the parameters that fit your needs (e.g. log level, handlers, etc.).
