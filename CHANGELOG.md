# Changelog

This file reflects the evolution of the Hecate API, from one version to
another.

## Version 1.0.2

+ Make easier the access to the Logger used by the API

## Version 1.0.1

+ Replace the use of Thread by the use of CompletableFuture

## Version 1.0

+ First implementation of channels
+ First implementation of routines
+ First implementation of synchronization tools
